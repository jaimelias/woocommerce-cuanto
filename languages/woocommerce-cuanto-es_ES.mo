��          �       �       �      �                /     >  $   K  0   p  W   �  E   �     ?     E     `  	   {  �  �     !     9  %   F     l     �  .   �  3   �  E   �  I   9     �  '   �     �  	   �   Awaiting %s payment Description Enable %s Gateway Enable/Disable Instructions Let your customers pay using Cuanto. Let your customers pay using Visa or Mastercard. Make your payment with Visa or Mastercard and send us the payment receipt (screenshot). The payment instructions will be sent as soon as your order is ready. Title Woocommerce Cuanto Gateway https://www.jaimelias.com/ jaimelias Project-Id-Version: Woocommerce Cuanto Gateway
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-25 22:16+0000
PO-Revision-Date: 2020-05-25 22:15+0000
Last-Translator: 
Language-Team: Español
Language: es_ES
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.0; wp-5.4.1 En espera de pago de %s Descripción Habilitar pasarela de pagos %s Cuanto Habilitar/Desabilitar Instrucciones Deja que tus clientes te paguen usando Cuanto. Deja que tus clientes paguen con Visa o Mastercard. Haz tu pago con Visa o Mastercard y envíanos el recibo (screenshot). Te enviaremos las instrucciones de pago tan pronto esté listo tu pedido. Título Pasarela de pagos Cuanto en Woocommerce https://www.jaimelias.com/ jaimelias 