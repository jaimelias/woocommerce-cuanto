<?php
/**
 *
 * Plugin Name: Woocommerce Cuanto Gateway
 * Description: Let your customers pay using Cuanto.
 * Version: 1.0
 * Author: jaimelias
 * Author URI: https://www.jaimelias.com/
 * Text Domain: woocommerce-cuanto
 * Domain Path: /languages
 */

if (!defined('ABSPATH'))
{
    return;
}

$active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );

if ( in_array( 'woocommerce/woocommerce.php',  $active_plugins) )
{
	if (!function_exists('is_woocommerce_activated'))
	{
		function is_woocommerce_activated()
		{
			if (class_exists('woocommerce'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	add_action('init', 'Cuanto_Gateway_textdomain');

	function Cuanto_Gateway_textdomain()
	{
		load_plugin_textdomain(
			'woocommerce-cuanto',
			false,
			dirname( plugin_basename( __FILE__ )). '/languages'
		);		
	}

	add_filter('woocommerce_payment_gateways', 'Cuanto_Gateway_Add_Class');

	function Cuanto_Gateway_Add_Class($gateways)
	{
		$gateways[] = 'WC_Cuanto_Gateway'; // your class name is here
		return $gateways;
	}

	add_action('plugins_loaded', 'Cuanto_Gateway_Init');

	function Cuanto_Gateway_Init()
	{
		class WC_Cuanto_Gateway extends WC_Payment_Gateway
		{
			private $domain;
			
			public function __construct()
			{
				$this->id = 'cuanto';
				$this->icon = '';
				$this->has_fields = false;
				$this->method_title = 'Cuanto';
				$this->method_description = esc_attr(sprintf(__('Let your customers pay using Visa or Mastercard.', 'woocommerce-cuanto'), $this->method_title));
				$this->supports = array(
					'products'
				);
				$this->init_form_fields();

				// Load the settings.
				$this->init_settings();
				$this->title = $this->get_option('title');
				$this->description = $this->get_option('description');
				$this->instructions = $this->get_option('instructions');
				$this->enabled = $this->get_option('enabled');
				$this->init();
			}
			
			public function init()
			{
				add_action( 'wp_head', array($this, 'colors'), 1);
				add_action('woocommerce_update_options_payment_gateways_' . $this->id, array(
					$this,
					'process_admin_options'
				));
				add_action('woocommerce_email_before_order_table', array(
					$this,
					'email_instructions'
				) , 10, 3);
				add_action('woocommerce_thankyou_' . $this->id, array(
					$this,
					'thankyou_page'
				));				
			}
			
			public static function colors()
			{
				echo '<style type="text/css">#payment ul.payment_methods>li.payment_method_cuanto,#payment ul.payment_methods>li.payment_method_cuanto:hover{background-color:#1a1f71}#payment ul.payment_methods li.payment_method_cuanto input[type=radio]:first-child:checked+label:before{color:#fdbb0a}#payment ul.payment_methods>li.payment_method_cuanto label {color: #fff}#payment ul.payment_methods>li.payment_method_cuanto > div.payment_method_cuanto{background-color: #fdbb0a; color: #000}</style>';
			}
			public function init_form_fields()
			{
				$args = array(
					'enabled' => array(
						'title' => __('Enable/Disable', 'woocommerce-cuanto'),
						'label' => esc_attr(sprintf(__('Enable %s Gateway', 'woocommerce-cuanto'), $this->method_title)),
						'type' => 'checkbox',
						'description' => '',
						'default' => 'no'
					) ,
					'title' => array(
						'title' => esc_attr(__('Title', 'woocommerce-cuanto')),
						'type' => 'text',
						'default' => esc_attr(__('Visa or Mastercard', 'woocommerce-cuanto'))
					) ,
					'description' => array(
						'title' => esc_attr(__('Description', 'woocommerce-cuanto')),
						'type' => 'textarea',
						'default' => esc_attr(__('Make your payment with Visa or Mastercard and send us the payment receipt (screenshot).', 'woocommerce-cuanto'))
					) ,
					'instructions' => array(
						'title' => esc_attr(__('Instructions', 'woocommerce-cuanto')),
						'type' => 'textarea',
						'default' => esc_attr(sprintf(__('The payment instructions will be sent as soon as your order is ready.', 'woocommerce-cuanto'), $this->method_title))
					)		
				);

				$this->form_fields = apply_filters('wc_offline_form_fields', $args);
			}

			public function process_payment($order_id)
			{
				global $woocommerce;
				$order = new WC_Order( $order_id );

				$order->update_status('on-hold', sprintf(__('Awaiting %s payment', 'woocommerce-cuanto'), $this->method_title));

				$woocommerce->cart->empty_cart();

				return array(
					'result' => 'success',
					'redirect' => esc_url($this->get_return_url( $order ))
				);
			}

			public function thankyou_page()
			{
				if ($this->instructions)
				{
					echo '<h3>' . wp_kses_post(wpautop(wptexturize($this->instructions))) . '</h3>';
				}
			}

			public function email_instructions($order, $sent_to_admin, $plain_text = false)
			{
				if ($this->instructions && !$sent_to_admin && $this->id === $order->get_payment_method() && $order->has_status('on-hold'))
				{					
					echo wp_kses_post(wpautop(wptexturize($this->instructions)) . PHP_EOL);
				}
			}

		}
	}	
}

